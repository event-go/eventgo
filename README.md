# README #

This README would normally document whatever steps are necessary to get your application up and running.

Проект "EventGO" помогает людям создавать мероприятия, находить единомышленников,
и любых людей с вашего города, которые готовы составить компанию на каком-либо событии. 
Так же можно отслеживать количество людей, готовых прийти на ивент, смотреть историю 
прошедших событий в профиле организатора

Цель проекта - изучение фреймворка Spring, а именно: Inversion of Control, 
Dependency Injection, жизненный цикл бина, контроллеры, аннотации.

Задачи:

1) Создать все классы и файлы для отображения главной страницы сайта в браузере. 
Сделать красивый вид для страницы.
2) Создать раздел, на котором будут отображаться посты из базы данных
3) Создать базу данных и выполнить подключение к ней через Java Spring и технологию JDBC.
4) Создать форму, прописать обработчики для получения данных, а также сохранять новые статьи 
в БД Postgres.
5) Добавить динамические страницуы, на которых будут отображаться различные статьи в 
зависимости от параметра, переданного в URL адрес.
6) Добавить возможность для редактирования и удаления статей из базы данных.

В ходе проета была реализована конфигурация приложения (бины, component scan, настройка 
Thymeleaf), контроллеры и DAO. Внедрены зависимости бинов, реализован паттерн MVC, 
передача данных от контроллера к представлению, протокол HTTP, 
основы построения RESTful API приложения, реализованы методы GET и POST.

Стек использованных технологий: Java, Spring Boot, Spring Security, JDBC, HTML, CSS, 
REST API, thymeleaf

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact